Package.describe({
  name: 'znewsham:blaze-explorer-driver',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md',
  debugOnly: true
});

Package.onUse(function(api) {
  api.versionsFrom('1.4');
  api.use('ecmascript');
  api.use("underscore");
  api.use(["znewsham:blaze-explorer", "kadira:blaze-layout", "kadira:flow-router", "reactive-dict", "blaze", "tracker"], "client");
  api.mainModule('blaze-explorer-driver.js', "client");
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('blaze-explorer-driver');
  api.mainModule('blaze-explorer-driver-tests.js');
});
