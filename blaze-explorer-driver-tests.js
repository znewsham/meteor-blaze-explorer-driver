// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by blaze-explorer-driver.js.
import { name as packageName } from "meteor/blaze-explorer-driver";

// Write your tests here!
// Here is an example.
Tinytest.add('blaze-explorer-driver - example', function (test) {
  test.equal(packageName, "blaze-explorer-driver");
});
