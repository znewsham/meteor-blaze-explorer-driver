import "meteor/blaze";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
import { FlowRouter } from "meteor/kadira:flow-router";
import "meteor/znewsham:blaze-explorer";

FlowRouter.route("/__blaze-explorer/:collectionName?/:componentName?/:caseName?", {
  name: "__test",
  action() {
    BlazeLayout.render(
      "__blazeExplorer_layout",
      {
        data: {
          otherData: "test",
          collectionName: FlowRouter.getParam("collectionName"),
          componentName: FlowRouter.getParam("componentName"),
          caseName: FlowRouter.getParam("caseName")
        }
      }
    );
  }
});


export function runTests() {
  BlazeLayout.render(
    "__blazeExplorer_layout",
    {
      data: {
        otherData: "test",
        collectionName: FlowRouter.getParam("collectionName"),
        componentName: FlowRouter.getParam("componentName"),
        caseName: FlowRouter.getParam("caseName")
      }
    }
  );
}
